# Allegro 4 Pong Game
### By ENIGMATIC0

## RESUME:
 
 
          A L L E G R O   P O N G   G A M E         
       VERSION FOR MS-DOS, USING ALLEGRO 4.2.2      
 
                                                    
      CREATED BY ENIGMATIC0, IN C (USING DJGPP)    
              AND COMPILED USING GNU C              
                                                    
 

  REDISTRIBUTION OF THIS SOFTWARE IS ALLOWED, "AS IT
   IS". SELLING OR MONETIZING THIS SOFTWARE, OR ANY OF
   IT'S PARTS, IS FORBIDDEN.
  
   YOU CAN MODIFY AND REDISTRIBUTE THIS GAME, ALSO "AS
   IT IS".
  
   ----------------------------------------------------
  
   PONG is a classic arcade game, originally developed
   by Atari Inc, in 1972. Becoming one of the earliest
   and most famous games ever made. Because of it's fame,
   many companies developed clones of this game, into
   'stand-alone' consoles.
  
   In PONG, each player control a "paddle" that can move
   left or right. The goal of the game, is to bounce back
   a ball, that is being disputed, and make it tresspass
   the other player's screen bound (score).
  
   Usually, there is a score limit. The first player that
   reach this score limit, wins the round.
  
   ---------------------------------------------------
  
                   GAME DESCRIPTION
  
   The goal of this game is to be the first to score 15
   points. The player (player 1), controls the paddle in the
   bottom with the ARROW KEYS. The AI (player 2), controls
   the one in the upper part of the screen.
  
   To start the game, press the SPACE key when the ball is in
   the neutral position. This will make the ball move.
  
   To score points, make the ball go beyond the Player 2's screen
   bound. But make sure he doesn't score by using your paddle to
   bounce back the ball when it's moving towards you.
  
   You can also hold the LEFT SHIFT key to slow down your paddle.
   this will make your movement smoother, but also slower.
   If you press the LEFT CONTROL key, you can also see the trajectory
   of the ball. This can be helpfull sometimes, but it is considered
   "cheating".
  
  
## PRECOMPILED BINARIES:
  
  Download the precompiled binaries from here: https://www.dropbox.com/s/pqwswdgmv17e4p6/PONGDOS.zip?dl=1
  
  ## INSTALLATION:
  
  You need to meet at least one of these requeriments:
  
  - A computer with MS-DOS (Either a pure MS-DOS (6.0+), or Windows 3.1x to Windows 9x/Me/2000)
  
  - DosBOX (http://www.dosbox.com/)
  
  - A Virtual Machine (Such as VMWare or Oracle VirtualBox, https://www.virtualbox.org/)
  with one of the supported systems installed on it (MS-DOS 6.0+/Windows3.1x/9x/Me/2000).
  
  The NTVDM virtual machine included with Windows XP and (optionally installable) with Windows 7/8/8.1/10
  CAN NOT RUN THIS GAME, as it does not include VESA support. Allegro4 rely on VESA to display graphics.
  
  The DosBOX version for Android DOES NOT support this game as for now.
  
  Once you have your environment set up, just decompress the game folder anywhere you want. Inside the game
  folder, there is a folder named DPMI. The game REQUIRES the files inside to be able to run, as this is
  the DPMI Server that allows Allegro4 to run with extended memory support.
  
  All you need to do is to copy or move the content inside, to the same folder the executable is in.
  Example:
  
> move DPMI\*.* .
  
  Once this is done, you can run the game from the command line as usual.
  
## FROM SOURCE
  
  You need MinGW for DOS. The easiest and fastest way to set up a development environment for
  this system is by installing DJGPP, which includes everything: http://www.delorie.com/djgpp/
  
  You also need the Allegro4 library. You can get it from here: http://liballeg.org/old.html
  
  On DosBOX, modifying the autoexec.bat won't work since the commands there are not executed. Instead, DosBOX
  uses it's own configuration file, which includes an AUTOEXEC section for commands you want to run at
  startup. That's where you must register your environment variables to work with DJGPP. Example:
  
MOUNT C C:\DOSBOX
SET PATH=C:\DJGPP\BIN;%PATH%
SET DJGPP=C:\DJGPP\DJGPP.ENV
C:

  Once everything is set up correctly, you can compile the code like this:

> gcc MAIN.c -o PONG -lalleg

  It will generate the compiled executable PONG.EXE in the current directory.
