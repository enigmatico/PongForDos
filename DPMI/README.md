CWSDPMI is Copyright (C) 1995-2000  Charles W Sandmann 

These files correspond to the DPMI server required by PONG.EXE. Allegro 4 games
require a DPMI server for extended memory when running games on DOS mode.

Simply copy these files in the same folder as the executable in order to make
it work.